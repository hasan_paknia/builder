<?php
namespace Tests\Managers;

use App\Managers\Builder;

class BuilderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Inputs
     * @var array
     */
    protected $inputs;

    /**
     * Manager class
     * @var Builder 
     */
    protected $manager;

    public function setUp()
    {
        parent::setUp();

        $this->manager = new Builder();

        $this->inputs = [
            25,
            80,
            40,
            25,
            57,
            15,
            35,
            20,
            65,
            70,
            30,
            45
        ];
    }

    public function testSuccessfulCall()
    {
        // feed inputs
        foreach ($this->inputs as $monthIndex => $amount) {
            $this->manager->build($monthIndex, $amount);
        }

        $actual = $this->manager->getTimeLine();
        $expected = [
            'Januray'   => [40, 5],
            'Febuary'   => [40, 20],
            'March'     => [40, 0],
            'April'     => [40, 0],
            'May'       => [40, 2],
            'June'      => [40, 0],
            'July'      => [40, 0],
            'August'    => [40, 0],
            'September' => [40, 0],
            'October'   => [40, 5],
            'November'  => [35, 0],
            'December'  => [40, 0]
        ];

        $this->assertEquals($expected, $actual);

        echo PHP_EOL;
        echo $this->manager;
        echo PHP_EOL;
    }

    /**
     * @expectedException \Exception
     */
    public function testCallWithException()
    {
        // any number more than 95 throws an exception
        $this->inputs[1] = 96;

        // feed inputs
        foreach ($this->inputs as $monthIndex => $amount) {
            $this->manager->build($monthIndex, $amount);
        }
    }
}
