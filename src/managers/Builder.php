<?php
namespace App\Managers;

/**
 * Computer Builder Class
 *
 * @author hassan
 */
class Builder
{

    /**
     * List of months
     * @var array
     */
    const MONTHS = [
        'Januray',
        'Febuary',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    /**
     * Cost values
     * @var array
     */
    protected $costs = [15, 40];

    /**
     * Quantity amounts
     * @var array
     */
    protected $quantities = [40, 20];

    /**
     * Hosting cost
     * @var int
     */
    protected $hostingCost = 5;

    /**
     * Timeline of production
     * @var array
     */
    protected $timeline;

    /**
     * Builder Constructor
     */
    function __construct()
    {
        $this->reset();
    }

    /**
     * Resets timeline to the initial state
     * @return Builder
     */
    public function reset()
    {
        // reset timeline
        $this->timeline = [];

        // create skeleton of each month
        $skeleton = array_fill(0, count($this->costs), 0);

        // loop through month to set default values
        foreach (self::MONTHS as $index => $month) {
            $this->timeline[$month] = $skeleton;
        }

        return $this;
    }

    /**
     * Returns current status of timeline
     * @return array
     */
    public function getTimeLine()
    {
        return $this->timeline;
    }

    /**
     * Prints the timeline of the object
     */
    public function __toString()
    {
        // define output
        $output = '';

        // define total
        $total = 0;

        // loop through timeline to print build plan
        foreach ($this->timeline as $month => $plan) {
            // define month total
            $monthTotal = 0;

            $output .= "{$month}: ";

            // loop through number of builds per cost for this month
            foreach ($plan as $index => $value) {
                // if no computers is needed to get built by this price
                if ($value == 0) {
                    continue;
                }

                $monthTotal += (($this->costs[$index] + $this->hostingCost) * $value);
                // print message
                $output .= "{$value} for \${$this->costs[$index]}, ";
            }

            $output .= "with mothly total of \${$monthTotal}" . PHP_EOL;

            $total += $monthTotal;
        }

        $output .= "Total cost for the year is \${$total}";

        return $output;
    }

    /**
     * This function decides how needed $amount of computers should be built
     * It starts from the same month with the first (least) price going backward 
     * It works fine under assumption of receiving valid inputs
     * @param int $monthIndex stating from 0 to 11
     * @param type $amount of computers need to be built
     * @return bool | string of error
     * \Exception if building requested number of computers is not possible
     */
    public function build($monthIndex, $amount)
    {
        // loop through costs to define how many should be bbuilt per month
        foreach ($this->costs as $key => $value) {
            // get current month maximum quantity
            $quantity = $this->quantities[$key];

            // copy month index
            $monthStartIndex = $monthIndex;
            
            // start from same month
            while ($monthStartIndex >= 0) {
                // get current month available quantity
                $availableQuantity = $quantity - $this->timeline[self::MONTHS[$monthStartIndex]][$key];

                // if there are no available quantity
                if ($availableQuantity === 0) {
                    // try previous month 
                    --$monthStartIndex;
                    continue;
                }

                // there are some availablities
                // if there are enough availability to build it all
                if ($availableQuantity >= $amount) {
                    // add amount to timeline
                    $this->timeline[self::MONTHS[$monthStartIndex]][$key] += $amount;
                    // we could build this amount successfully
                    return true;
                }

                // we could not build it all
                // this month's quantity is full now, we continue to process the rest
                $this->timeline[self::MONTHS[$monthStartIndex]][$key] = $quantity;

                // reduce the total amount
                $amount -= $availableQuantity;

                // try previous month 
                --$monthStartIndex;
            }
        }

        // if we are at this point and we have not been able to build it up, we can not build this number on time
        throw new \Exception("We can build it for following inputs: " . var_export(func_get_args(), true));
    }
}
